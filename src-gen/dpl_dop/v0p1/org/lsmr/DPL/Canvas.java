package dpl_dop.v0p1.org.lsmr.DPL;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.util.*;
import java.awt.event.*;
import javax.swing.JComponent;
import java.awt.Point;
/*** added by dCanvas* modified by dLineCanvas
 */
@SuppressWarnings("serial")
public class Canvas extends JComponent implements MouseListener,
MouseMotionListener {
	Point start, end;
	public FigureTypes figureSelected = FigureTypes.NONE;
	public Canvas() {
		this.setDoubleBuffered(true);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}
	public void selectedFigure(FigureTypes fig) {
		figureSelected = fig;
	}
	public void mouseClicked(MouseEvent e) {
	}
	public void mouseEntered(MouseEvent e) {
	}
	public void mouseExited(MouseEvent e) {
	}
	public void mouseMoved(MouseEvent e) {
	}
	/*** added by dLineCanvas
	 */
	protected List<Line> lines = new LinkedList<Line>();
	/*** added by dLineCanvas
	 */
	protected Line newLine = null;
	/*** added by dLineCanvas
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
		for(Line l : lines) {
			l.paint(g);
		}
	}
	/*** added by dLineCanvas
	 */
	public void mousePressed(MouseEvent e) {
		switch(figureSelected) {
			case LINE : mousePressedLine(e);
			break;
		}
	}
	/*** added by dLineCanvas
	 */
	public void mouseReleased(MouseEvent e) {
		switch(figureSelected) {
			case LINE : mouseReleasedLine(e);
			break;
		}
	}
	/*** added by dLineCanvas
	 */
	public void mouseDragged(MouseEvent e) {
		switch(figureSelected) {
			case LINE : mouseDraggedLine(e);
			break;
		}
	}
	/*** added by dLineCanvas
	 */
	public void mousePressedLine(MouseEvent e) {
		if(newLine == null) {
			start = new Point(e.getX(), e.getY());
			newLine = new Line(start);
			lines.add(newLine);
		}
	}
	/*** added by dLineCanvas
	 */
	public void mouseDraggedLine(MouseEvent e) {
		newLine.setEnd(new Point(e.getX(), e.getY()));
		repaint();
	}
	/*** added by dLineCanvas
	 */
	public void mouseReleasedLine(MouseEvent e) {
		newLine = null;
	}
}