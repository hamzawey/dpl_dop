package dpl_dop.v0p1.org.lsmr.DPL;

import java.awt.*;
/*** added by dLine
 */
public class Line {
	private Point startPoint, endPoint;
	public void paint(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
	}
	public Line(Point start) {
		startPoint = start;
	}
	public void setEnd(Point end) {
		endPoint = end;
	}
	public Point getStart() {
		return startPoint;
	}
	public Point getEnd() {
		return endPoint;
	}
}