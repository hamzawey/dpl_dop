package dpl_dop.v0p4.org.lsmr.DPL;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import java.util.ArrayList;
/*** added by dMain* modified by dLineMain* modified by dRectangleMain*
modified by dRectangleLineMain* modified by dColor* modified by dLineColor*
modified by dRectangleColor* modified by dLineRectangleColor
 */
@SuppressWarnings("serial")
public class Main extends JFrame {
	private static final int WIDTH = 800;
	private static final int HEIGHT = 600;
	protected JPanel toolPanel = new JPanel();
	protected Canvas canvas = new Canvas();
	protected ArrayList<Component> components;
	public void initAtoms(ArrayList<Component> components) {
		for(Component component : components) {
			if(component.item instanceof JButton) component.item = new
			JButton(component.name);
		}
	}
	Container contentPane;
	public void initLayout() {
		contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		toolPanel.setLayout(new BoxLayout(toolPanel, BoxLayout.Y_AXIS));
	}
	public void initContentPane(ArrayList<Component> components) {
		for(Component component : components) toolPanel.add(component.item);
		contentPane.add(toolPanel, BorderLayout.WEST);
		contentPane.add(canvas, BorderLayout.CENTER);
	}
	public void init() {
		initAtoms();
		initLayout();
		initContentPane(components);
		initListeners();
	}
	public Main(String appTitle) {
		super(appTitle);
		init();
		addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			});
		setVisible(true);
		setSize(WIDTH, HEIGHT);
		setResizable(true);
		validate();
	}
	public static void main(String [] args) {
		new Main("Draw Product Line");
	}
	/*** added by dLineMain
	 */
	private static final String lineText = "Line";
	/*** added by dRectangleMain
	 */
	private static final String rectText = "Rectangle";
	/*** added by dColor
	 */
	private static final Vector<String> colors = new Vector<String>();
	/*** added by dColor
	 */
	private static final String red = "Red";
	/*** added by dColor
	 */
	private static final String green = "Green";
	/*** added by dColor
	 */
	private static final String blue = "Blue";
	/*** added by dColor
	 */
	private static final String black = "Black";
	/*** added by dColor
	 */
	JComboBox colorsBox;
	/*** added by dColor
	 */
	protected JPanel colorsPanel = new JPanel();
	/*** added by dLineRectangleColor
	 */
	public void initAtoms() {
		components = new ArrayList<Component>();
		Component component = new Component();
		component.name = rectText;
		component.item = new JButton(rectText);
		component.type = FigureTypes.RECT;
		components.add(component);
		component = new Component();
		component.name = lineText;
		component.item = new JButton(lineText);
		component.type = FigureTypes.LINE;
		components.add(component);
		colors.add(black);
		colors.add(red);
		colors.add(green);
		colors.add(blue);
		colorsBox = new JComboBox(colors);
		colorsBox.setSelectedIndex(0);
		colorsPanel.add(colorsBox);
		component = new Component();
		component.item = colorsPanel;
		component.type = FigureTypes.NONE;
		components.add(component);
	}
	/*** added by dLineRectangleColor
	 */
	public void initListeners() {
		for(Component item : components) {
			if(item.item instanceof JButton) {(( JButton )
					item.item).addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							canvas.selectedFigure(item.type);
						}
					});
			}
			else if(item.item instanceof JPanel) {
				JPanel panel = ( JPanel ) item.item;(( JComboBox )
					panel.getComponent(0)).addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							if(e.getSource() == colorsBox) {
								String colorString =(( String ) colorsBox.getSelectedItem());
								canvas.setColor(colorString);
							}
						}
					});
			}
		}
	}
}