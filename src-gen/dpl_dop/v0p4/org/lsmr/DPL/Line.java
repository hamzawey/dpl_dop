package dpl_dop.v0p4.org.lsmr.DPL;

import java.awt.*;
/*** added by dLine* modified by dLineColor
 */
public class Line {
	private Point startPoint, endPoint;
	public Line(Point start) {
		startPoint = start;
	}
	public void setEnd(Point end) {
		endPoint = end;
	}
	public Point getStart() {
		return startPoint;
	}
	public Point getEnd() {
		return endPoint;
	}
	/*** added by dLineColor
	 */
	private Color color;
	/*** added by dLineColor
	 */
	public void paint(Graphics g) {
		g.setColor(color);
		g.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
	}
	/*** added by dLineColor
	 */
	public Line(Color color, Point start) {
		startPoint = start;
		this.color = color;
	}
}