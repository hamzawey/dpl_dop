package dpl_dop.v0p4.org.lsmr.DPL;

/*** added by dFigureTypes* modified by dLineCanvas* modified by
dRectangleCanvas
 */
public enum FigureTypes {
	NONE, /*** added by dLineCanvas
	 */
	LINE, /*** added by dRectangleCanvas
	 */
	RECT
};