package dpl_dop.v1p2.org.lsmr.DPL;

/*** added by dFigureTypes* modified by dLineCanvas* modified by
dRectangleCanvas
 */
public enum FigureTypes {
	NONE, /*** added by dLineCanvas
	 */
	LINE, /*** added by dRectangleCanvas
	 */
	RECT
};