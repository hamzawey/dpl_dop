package dpl_dop.v0p3.org.lsmr.DPL;

import java.awt.Color;
import java.awt.Graphics;
/*** added by dRectangle
 */
public class Rectangle {
	private int x, y, width, height;
	private int dx, dy;
	private int x2, y2;
	public void paint(Graphics g) {
		int cornerX = x, cornerY = y;
		if(dy < 0) {
			if(dx >= 0) {
				cornerX = x;
				cornerY = y2;
			}
			else {
				cornerX = x2;
				cornerY = y2;
			}
		}
		else {
			if(dx >= 0) {
				cornerX = x;
				cornerY = y;
			}
			else {
				cornerX = x2;
				cornerY = y;
			}
		}
		g.setColor(Color.BLACK);
		g.drawRect(cornerX, cornerY, width, height);
	}
	public Rectangle(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public void setEnd(int newX, int newY) {
		width = StrictMath.abs(newX - x);
		height = StrictMath.abs(newY - y);
		dx = newX - x;
		dy = newY - y;
		x2 = newX;
		y2 = newY;
	}
	public void updateCorner() {
		int cornerX = x, cornerY = y;
		if(dy < 0) {
			if(dx >= 0) {
				cornerX = x;
				cornerY = y2;
			}
			else {
				cornerX = x2;
				cornerY = y2;
			}
		}
		else {
			if(dx >= 0) {
				cornerX = x;
				cornerY = y;
			}
			else {
				cornerX = x2;
				cornerY = y;
			}
		}
		x = cornerX;
		y = cornerY;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
}