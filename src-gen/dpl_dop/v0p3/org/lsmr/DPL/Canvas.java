package dpl_dop.v0p3.org.lsmr.DPL;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.util.*;
import java.awt.event.*;
import javax.swing.JComponent;
import java.awt.Point;
/*** added by dCanvas* modified by dRectangleCanvas
 */
@SuppressWarnings("serial")
public class Canvas extends JComponent implements MouseListener,
MouseMotionListener {
	Point start, end;
	public FigureTypes figureSelected = FigureTypes.NONE;
	public Canvas() {
		this.setDoubleBuffered(true);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}
	public void selectedFigure(FigureTypes fig) {
		figureSelected = fig;
	}
	public void mouseClicked(MouseEvent e) {
	}
	public void mouseEntered(MouseEvent e) {
	}
	public void mouseExited(MouseEvent e) {
	}
	public void mouseMoved(MouseEvent e) {
	}
	/*** added by dRectangleCanvas
	 */
	protected List<Rectangle> rects = new LinkedList<Rectangle>();
	/*** added by dRectangleCanvas
	 */
	protected Rectangle newRect = null;
	/*** added by dRectangleCanvas
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
		for(Rectangle r : rects) {
			r.paint(g);
		}
	}
	/*** added by dRectangleCanvas
	 */
	public void mousePressed(MouseEvent e) {
		switch(figureSelected) {
			case RECT : mousePressedRect(e);
			break;
		}
	}
	/*** added by dRectangleCanvas
	 */
	public void mouseReleased(MouseEvent e) {
		switch(figureSelected) {
			case RECT : mouseReleasedRect(e);
			break;
		}
	}
	/*** added by dRectangleCanvas
	 */
	public void mouseDragged(MouseEvent e) {
		switch(figureSelected) {
			case RECT : mouseDraggedRect(e);
			break;
		}
	}
	/*** added by dRectangleCanvas
	 */
	public void mousePressedRect(MouseEvent e) {
		if(newRect == null) {
			newRect = new Rectangle(e.getX(), e.getY());
			rects.add(newRect);
		}
	}
	/*** added by dRectangleCanvas
	 */
	public void mouseDraggedRect(MouseEvent e) {
		newRect.setEnd(e.getX(), e.getY());
		repaint();
	}
	/*** added by dRectangleCanvas
	 */
	public void mouseReleasedRect(MouseEvent e) {
		newRect = null;
	}
}