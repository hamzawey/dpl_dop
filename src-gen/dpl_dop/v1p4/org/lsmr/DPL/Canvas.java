package dpl_dop.v1p4.org.lsmr.DPL;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.util.*;
import java.awt.event.*;
import javax.swing.JComponent;
import java.awt.Point;
/*** added by dCanvas* modified by dRectangleCanvas* modified by
dCanvasFilled* modified by dCanvasRectangleFilled* modified by dColor*
modified by dRectangleColor* modified by dRectangleColorFill
 */
@SuppressWarnings("serial")
public class Canvas extends JComponent implements MouseListener,
MouseMotionListener {
	Point start, end;
	public FigureTypes figureSelected = FigureTypes.NONE;
	public Canvas() {
		this.setDoubleBuffered(true);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}
	public void selectedFigure(FigureTypes fig) {
		figureSelected = fig;
	}
	public void mouseClicked(MouseEvent e) {
	}
	public void mouseEntered(MouseEvent e) {
	}
	public void mouseExited(MouseEvent e) {
	}
	public void mouseMoved(MouseEvent e) {
	}
	/*** added by dRectangleCanvas
	 */
	protected List<Rectangle> rects = new LinkedList<Rectangle>();
	/*** added by dRectangleCanvas
	 */
	protected Rectangle newRect = null;
	/*** added by dRectangleCanvas
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
		for(Rectangle r : rects) {
			r.paint(g);
		}
	}
	/*** added by dRectangleCanvas
	 */
	public void mousePressed(MouseEvent e) {
		switch(figureSelected) {
			case RECT : mousePressedRect(e);
			break;
		}
	}
	/*** added by dRectangleCanvas
	 */
	public void mouseReleased(MouseEvent e) {
		switch(figureSelected) {
			case RECT : mouseReleasedRect(e);
			break;
		}
	}
	/*** added by dRectangleCanvas
	 */
	public void mouseDragged(MouseEvent e) {
		switch(figureSelected) {
			case RECT : mouseDraggedRect(e);
			break;
		}
	}
	/*** added by dRectangleCanvas
	 */
	public void mouseDraggedRect(MouseEvent e) {
		newRect.setEnd(e.getX(), e.getY());
		repaint();
	}
	/*** added by dRectangleCanvas
	 */
	public void mouseReleasedRect(MouseEvent e) {
		newRect = null;
	}
	/*** added by dCanvasFilled
	 */
	protected boolean filled = false;
	/*** added by dCanvasFilled
	 */
	public void setFilled(boolean filled) {
		this.filled = filled;
	}
	/*** added by dColor
	 */
	protected Color color = Color.BLACK;
	/*** added by dColor
	 */
	public void setColor(String colorString) {
		if(colorString.equals("Red")) color = Color.red;
		else if(colorString.equals("Green")) color = Color.green;
		else if(colorString.equals("Blue")) color = Color.blue;
		else color = Color.black;
	}
	/*** added by dRectangleColorFill
	 */
	public void mousePressedRect(MouseEvent e) {
		if(newRect == null) {
			newRect = new Rectangle(color, e.getX(), e.getY(), filled);
			rects.add(newRect);
		}
	}
}